"""
n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!
"""
import functools
import math


sample = 10
problem = 100


def test(data) -> None:
    "Test assumptions made in the problem"
    value = solve(data)
    assert 27 == value, f"Error while testing assumptions 27 == {value}"


def solve(data):
    "Solve given problem"
    values = range(1, data+1)
    factorial = str(math.prod(values))
    result = sum(int(d) for d in factorial)
    return result


if "__main__" == __name__:
    test(sample)
    result = solve(problem)
    print(result)
