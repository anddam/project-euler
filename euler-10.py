"""
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
"""
from bisect import bisect
from itertools import count, takewhile
from math import isqrt


def prime_generator():
    primes = [2]
    yield 2
    for candidate in count(3, 2):
        if any(candidate % v == 0 for v in primes[:bisect(primes, isqrt(candidate))]):
            continue
        primes.append(candidate)
        yield candidate


if __name__ == "__main__":
    N = 2_000_000
    result = sum(takewhile(lambda x: x < N, prime_generator()))
    print(result)
