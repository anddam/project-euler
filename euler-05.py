"""
2520 is the smallest number that can be divided by each of the
numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of
the numbers from 1 to 20?
"""
from collections import defaultdict


def prime_factors(value):
    """Returns prime factors of value"""
    while value > 1:
        for candidate in range(2, value // 2):
            if value % candidate == 0:
                yield candidate
                value = value // candidate
                break
        else:
            yield value
            value = 1


def prime_factors_with_power(value):
    """Return each factor once with its exponent"""
    bin = defaultdict(int)
    for factor in prime_factors(value):
        bin[factor] += 1
    return bin


maximum_factors = defaultdict(int)
for r in range(2, 21):
    for base, power in prime_factors_with_power(r).items():
        maximum_factors[base] = max(maximum_factors[base], power)

result = 1
for base, power in maximum_factors.items():
    result *= base**power

print(f"{result}")
