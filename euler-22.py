"""
Using names.txt (right click and 'Save Link/Target As...'), a 46K text
file containing over five-thousand first names, begin by sorting it into
alphabetical order. Then working out the alphabetical value for each
name, multiply this value by its alphabetical position in the list to
obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN,
which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the
list. So, COLIN would obtain a score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
"""
from typing import List, Dict


sample = None
problem = None



def letter_value(letter: str) -> int:
    return ord(letter) - ord('A') + 1

def load_data(filename: str ="p022_names.txt") -> dict:
    with open(filename) as file:
        raw_data = file.readline().replace('"', "").split(",")
    sorted_data = { name: sum(map(lambda x: ord(x.upper()) - ord("@"), name)) for name in sorted(raw_data)}
    return sorted_data

def evaluate_score(data: dict) -> int:
    result = { key: {
        "idx": idx,
        "value": value,
        "score": value * (idx + 1),
        }
        for idx, (key, value) in enumerate(data.items())
               }

    return result


if "__main__" == __name__:
    data = load_data()
    score = evaluate_score(data)
    result=sum(obj["score"] for name, obj in score.items())

    print(result)
