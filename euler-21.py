"""
Let d(n) be defined as the sum of proper divisors of n (numbers less than n
which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and
each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55
and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and
142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
"""
import sys
from typing import Set


test_data = {
    220: {1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110},
    284: {1, 2, 4, 71, 142},
}
problem_value = 10000


def test(data) -> None:
    "Test assumptions made in the problem"
    for number, test_divisors in data.items():
        calculated_divisors = set(proper_divisors(number))
        assert calculated_divisors == test_divisors, f"Testing test_divisors of {number} failed, {calculated_divisors=} != {test_divisors=}"
        test_amicable = sum(test_divisors)
        calculated_amicable = get_amicable(number)
        assert calculated_amicable == test_amicable, f"Error validating {calculated_amicable=} {test_amicable=}"


def proper_divisors(number: int) -> Set[int]:
    """Return proper divisor set for a number
    """
    return (candidate for candidate in range(1, number // 2 + 1) if number % candidate == 0)


def get_amicable(number) -> int:
    """Check if a number has an amicable counterpart

    Return 0 if none found.
    """
    other_number = sum(proper_divisors(number))
    checksum = sum(proper_divisors(other_number))

    if other_number != number and checksum == number:
        return other_number
    else:
        return 0


def solve(number: int) -> int:
    "Solve given problem"
    visited = set()
    for value in range(0, number + 1):
        amicable = get_amicable(value)
        if value in visited:
            yield value
        elif amicable != 0:
            visited.add(amicable)
            yield value


if "__main__" == __name__:
    test(test_data)

    try:
        threshold = int(sys.argv[1])
    except ValueError:
        threshold = problem_default
    print(f"Solve for {threshold=}")

    amicables = list(solve(threshold))

    print(f"{amicables=}")
    print(f"{sum(amicables)=}")
