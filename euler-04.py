"""
A palindromic number reads the same both ways. The largest palindrome
made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
"""


def is_palindrome(value):
    value = str(value)
    size = len(value) // 2
    lhs = value[:size]
    rhs = value[-size:]
    return lhs == rhs[::-1]


def get_all_products(digits=3):
    for a in range(10 ** digits, 0, -1):
        for b in range(a, 0, -1):
            value = a * b
            if is_palindrome(value):
                yield value


print(max(get_all_products(3)))
