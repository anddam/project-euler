"""
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
"""


def collatz_sequence(number=1):
    while number != 1:
        yield number
        if number % 2 == 0:
            number = number // 2
        else:
            number = number * 3 + 1
        # number = number // 2 if (number % 2 == 0) else number * 3 + 1
    yield 1


def test():
    test_sequence = (13, 40, 20, 10, 5, 16, 8, 4, 2, 1)
    sequence = tuple(collatz_sequence(13))
    assert sequence == test_sequence, print(f"{sequence=}")


def solve(threshold=13):
    for start in range(1, threshold):
        yield sum(1 for _ in collatz_sequence(start)), start


if "__main__" == __name__:
    test()
    result = max(solve(1_000_000))
    print(result)
