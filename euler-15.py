"""
Starting in the top left corner of a 2×2 grid, and only being able to
move to the right and down, there are exactly 6 routes to the bottom
right corner.

[figure]: https://projecteuler.net/project/images/p015.png

How many such routes are there through a 20×20 grid?
"""
from itertools import count, islice


def tartaglia(line):
    return [0] + list(map(sum, zip(line, line[1:]))) + [0]


def paths_counter():
    # Start at 1 square
    line = [0, 1, 2, 1, 0]
    yield line[2]
    for _ in count(1):
        line = tartaglia(tartaglia(line))
        yield line[len(line) // 2]


if "__main__" == __name__:
    squares = 20
    result = islice(paths_counter(), squares-1, squares)
    for r in result:
        print(f"{r}")
