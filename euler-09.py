"""
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

    a^2 + b^2 = c^2

For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
"""
import sys
from math import prod

"""
Ranges allowed are
a in 1 .. (n - 3) // 3 == 332
b (a+1) .. (n - a - 1) // 2
c n - b -c
"""
def solve_problem(n=1000):
    for a in range(1, ((1000 - 3) // 3)):
        for b in range(a + 1, ((n - a -1) // 2) + 1):
            c = n - a - b
            if a ** 2 + b ** 2 == c ** 2:
                yield (a, b, c)
                print(f"({a}, {b}, {c})")

result = next(solve_problem_2())
print(prod(result))
