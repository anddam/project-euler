"""
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13,
we can see that the 6th prime is 13.

What is the 10 001st prime number?
"""
from sys import argv
from itertools import count, islice
from math import isqrt
from collections.abc import Iterable


def prime_numbers():
    """Generate prime numbers"""
    found_primes = list[int]()

    def prime_numbers_inner() -> Iterable[int]:
        for number in count(2):
            threshold = isqrt(number)
            if any(number % prime == 0 and prime <= threshold
                   for prime in found_primes):
                continue
            found_primes.append(number)
            yield number
    return prime_numbers_inner()


if __name__ == "__main__":
    try:
        position = int(argv[1])
    except (IndexError, ValueError):
        position = 10001

    result = next(islice(prime_numbers(), position, position + 1))
    print(f"{result}")
