#!/bin/sh
# new_template.sh <name>  create a new empty solution file from template

show_help() {
  echo "Usage:"
  echo "    new_template.sh <name>    create a solution file from template"
}

case $1 in
  "")
    show_help
    echo "Error: provide a file name"
    exit 1
    ;;
esac


cat <<EOF >$1
"""
Problem description.
"""

sample = None
problem = None


def test(data) -> None:
    "Test assumptions made in the problem"
    value = solve(data)
    assert None == solve(data), f"Error while testing assumptions {value=}"


def solve(data):
    "Solve given problem"
    return 0


if "__main__" == __name__:
    test(sample)
    result = solve(problem)
    print(result)
EOF
