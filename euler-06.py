"""
The sum of the squares of the first ten natural numbers is,

$$ 1^2 + 2^2 + … + 10^2 = 385 $$

The square of the sum of the first ten natural numbers is,

$$ (1 + 2 + … + 10)^2 = 3025 $$

Hence the difference between the sum of the squares of the first ten
natural numbers and the square of the sum is $3025 - 385 = 2640$ .

Find the difference between the sum of the squares of the first one
hundred natural numbers and the square of the sum.
"""
import sys

try:
    n = int(sys.argv[1])
except (ValueError, IndexError):
    n = 10

sum_of_squares = sum(map(lambda x: x ** 2, range(n + 1)))
square_of_sum = sum(range(n + 1)) ** 2
result = square_of_sum - sum_of_squares

print(result)
