"""
"If the numbers 1 to 5 are written out in words: one, two, three, four, five,
then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in
words, how many letters would be used?


NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20
letters. The use of "and" when writing out numbers is in compliance with British
usage.
"""

conversion_map = {
    1000: ("thousand", True, " and "),
    100: ("hundred", True, " and "),
    90: ("ninety", False, "-"),
    80: ("eighty", False, "-"),
    70: ("seventy", False, "-"),
    60: ("sixty", False, "-"),
    50: ("fifty", False, "-"),
    40: ("forty", False, "-"),
    30: ("thirty", False, "-"),
    20: ("twenty", False, "-"),
    19: ("nineteen", False, None),
    18: ("eighteen", False, None),
    17: ("seventeen", False, None),
    16: ("sixteen", False, None),
    15: ("fifteen", False, None),
    14: ("fourteen", False, None),
    13: ("thirteen", False, None),
    12: ("twelve", False, None),
    11: ("eleven", False, None),
    10: ("ten", False, None),
    9: ("nine", False, None),
    8: ("eight", False, None),
    7: ("seven", False, None),
    6: ("six", False, None),
    5: ("five", False, None),
    4: ("four", False, None),
    3: ("three", False, None),
    2: ("two", False, None),
    1: ("one", False, None),
}


def convert(number: int) -> str:
    result = ""
    for value, (name, require_multiplier, separator, ) in conversion_map.items():
        print(f"{value=}, ({name=}, {separator=})")
        if (quote := number // value) > 0:
            print(f"  {quote=}")
            number = number % (quote * value)
            if require_multiplier:
                multiplier = conversion_map[quote][0]
                result += f"{multiplier} "
            result += f"{name}"
            if number > 0:
                result += separator if separator else " "
    return result.strip()


def reduce_str(string: str) -> str:
    return string.lower().replace(" ", "").replace("-", "")


def count(string: str) -> int:
    return len(reduce_str(string))


def compare(a: str, b: str):
    return reduce_str(a) == reduce_str(b)


def test():
    value = convert(342)
    assert compare(value, "three hundred and forty-two"), f'{value=}   expected "three hundred and forty-two"'
    assert count(value) == 23, f'{len(value)=}'

    value = convert(115)
    assert compare(value, "one hundred and fifteen"), f'{value=}   expected "one hundred and fifteen"'
    assert count(value) == 20, f'{len(value)=}'


if "__main__" == __name__:
    assert test() is None
    print(count(" ".join(convert(_) for _ in range(1, 1001))))
