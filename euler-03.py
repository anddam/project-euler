"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""
import sys
from functools import cache


@cache
def is_prime(value):
    """Return true if value is prime"""
    for candidate in range(2, value // 2 + 1):
        if value % candidate == 0:
            return False
    return True


def prime_factors(value=1_000_000):
    """Generate prime factors of value"""
    while value > 1:
        for candidate in range(2, value // 2):
            if value % candidate == 0 and is_prime(candidate):
                yield candidate
                value = value // candidate
                break
        else:
            yield value
            value = 1


try:
    value = int(sys.argv[1])
except (ValueError, IndexError):
    value = 600851475143

print(max(list(prime_factors(value))))
