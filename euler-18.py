"""
By starting at the top of the triangle below and moving to adjacent
numbers on the row below, the maximum total from top to bottom is 23.
"""

problem_sample = (
    (3,),
    (7, 4),
    (2, 4, 6),
    (8, 5, 9, 3),
)

"""
That is, 3 + 7 + 4 + 9 = 23.

Find the maximum total from top to bottom of the triangle below:
"""

problem = (
    (75,),
    (95, 64),
    (17, 47, 82),
    (18, 35, 87, 10),
    (20,  4, 82, 47, 65),
    (19,  1, 23, 75,  3, 34),
    (88,  2, 77, 73,  7, 63, 67),
    (99, 65,  4, 28,  6, 16, 70, 92),
    (41, 41, 26, 56, 83, 40, 80, 70, 33),
    (41, 48, 72, 33, 47, 32, 37, 16, 94, 29),
    (53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14),
    (70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57),
    (91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48),
    (63, 66,  4, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31),
    (4,  62, 98, 27, 23,  9, 70, 98, 73, 93, 38, 53, 60,  4, 23),
)
"""
NOTE: As there are only 16384 routes, it is possible to solve this problem
by trying every route. However, Problem 67, is the same challenge with a
triangle containing one-hundred rows; it cannot be solved by brute force,
and requires a clever method! ;o)
"""


def test() -> None:
    "Test assumptions made in the problem"
    assert solve(problem_sample) == 23, "Error while testing assumptions"


def solve(payload: tuple) -> int:
    """Solve the problem minimizing operations

    The idea is to simplify the problem at hand, if you have the sample data
    you can reduce your problem to the simple case of having
    a) last line
    b) the best (max) path cost for each node of second to last line

    In this scenario it's easy to determine the solution,
    The first and last element of last line can only be accessed by first and
    last item of previous line (the one with cumulative path cost).
    Each other element of index i will have two potential parents, elements of
    index i and i-1 of previous line.
    The actual parent is the bigger one of the two.

    In order to achieve this scenario we need to scan line by line the data
    and carry only the best path until the currently observed line, at each step
    we have the previously described scenario.

    This is to say that we proceed iteratively rather than recursively.

    Intuitively we see we perform an evaluation per each item of the triangle,
    so the computation grows with its area.
    Analitically we are looping 1..k with k in 1..n, that is the sum of the
    first n numbers.

    The algorithm complexity is O(n^2).
    """
    previous = payload[0]
    for line in payload[1:]:
        cursor = [line[0] + previous[0]]
        cursor += [k + max(previous[index], previous[index + 1])
                   for (index, k) in enumerate(line[1:-1])]
        cursor += [line[-1] + previous[-1]]
        previous = cursor
    return max(cursor)


if "__main__" == __name__:
    test()
    result = solve(problem_sample)
    print(result)
